import {
  ADD_NOTE_REQUEST,
  FETCH_NOTES_REQUEST,
  FETCH_NOTES_SUCCESS,
  ADD_NOTE_SUCCESS,
} from '../actions/notesActions'

function notes (state = {
  isFetching: false,
  items: {},
}, action) {
  switch (action.type) {
    case ADD_NOTE_REQUEST:
      return {
        ...state,
        items: {
          ...state.items,
          ...action.note,
        },
      }
    case ADD_NOTE_SUCCESS:
      const otherNotes = {...state.items}
      const oldKey = Object.keys(action.note)[0]
      delete otherNotes[oldKey]

      return {
        ...state,
        items: {
          ...otherNotes,
          [action.id]: action.note[oldKey],
        },
      }
    case FETCH_NOTES_REQUEST:
      return {
        ...state,
        isFetching: true,
        items: {},
      }
    case FETCH_NOTES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.notes || {},
      }
    default:
      return state
  }
}

export default notes
