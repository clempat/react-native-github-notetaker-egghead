import { combineReducers } from 'redux'
import notes from './notes'
import user from './user'
import repos from './repos'

const rootReducer = combineReducers({
  notes,
  user,
  repos,
})

export default rootReducer
