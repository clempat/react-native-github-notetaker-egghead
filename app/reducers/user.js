import {
  FETCH_USER_BIO_REQUEST,
  FETCH_USER_BIO_SUCCESS,
  FETCH_USER_BIO_FAIL,
} from '../actions/userActions'

export default function user (state = {
  username: '',
  isFetching: false,
  error: '',
  payload: {},
}, action) {
  switch (action.type) {
    case FETCH_USER_BIO_REQUEST:
      return {
        ...state,
        username: action.username,
        isFetching: true,
      }
    case FETCH_USER_BIO_SUCCESS:
      return {
        ...state,
        username: '',
        isFetching: false,
        payload: action.user,
        error: '',
      }
    case FETCH_USER_BIO_FAIL:
      return {
        ...state,
        isFetching: false,
        error: action.error,
      }
    default:
      return state
  }
}
