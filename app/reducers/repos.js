import {
  FETCH_USER_REPOS_REQUEST,
  FETCH_USER_REPOS_SUCCESS,
} from '../actions/userActions'

function repos (state = {
  items: [],
  isFetching: false,
  error: '',
}, action) {
  switch (action.type) {
    case FETCH_USER_REPOS_REQUEST:
      return {
        ...state,
        isFetching: true,
        items: [],
      }
    case FETCH_USER_REPOS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.repos || [],
      }
    default:
      return state
  }
}

export default repos
