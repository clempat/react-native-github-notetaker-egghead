import React from 'react-native'

const {
  Text,
  View,
  Image,
  TouchableHighlight,
  StyleSheet,
} = React

const Dashboard = ({ goToProfile, goToRepos, goToNotes, user }) => {
  function makeBackground (btn) {
    const obj = {
      flexDirection: 'row',
      alignSelf: 'stretch',
      justifyContent: 'center',
      flex: 1,
    }

    switch (btn) {
      case 1:
        obj.backgroundColor = '#48BBEC'
        break
      case 2:
        obj.backgroundColor = '#E77AAE'
        break
      default:
        obj.backgroundColor = '#7588F4'
        break
    }

    return obj
  }

  return (
    <View style={styles.container}>
      <Image source={{ uri: user.avatar_url }} style={styles.image} />
      <TouchableHighlight
        onPress={goToProfile}
        underlayerColor="#88D4F5"
        style={makeBackground(1)}
      >
        <Text style={styles.buttonText}>View Profile</Text>
      </TouchableHighlight>
      <TouchableHighlight
        onPress={goToRepos}
        underlayerColor="#88D4F5"
        style={makeBackground(2)}
      >
        <Text style={styles.buttonText}>View Repos</Text>
      </TouchableHighlight>
      <TouchableHighlight
        onPress={goToNotes}
        underlayerColor="#88D4F5"
        style={makeBackground(3)}
      >
        <Text style={styles.buttonText}>View Notes</Text>
      </TouchableHighlight>
    </View>
  )
}

Dashboard.propTypes = {
  goToProfile: React.PropTypes.func.isRequired,
  goToRepos: React.PropTypes.func.isRequired,
  goToNotes: React.PropTypes.func.isRequired,
  user: React.PropTypes.object,
}

const styles = StyleSheet.create({
  container: {
    marginTop: 65,
    flex: 1,
  },
  image: {
    height: 350,
  },
  buttonText: {
    fontSize: 24,
    color: 'white',
    alignSelf: 'center',
  },
})

export default Dashboard
