import React, {
  View,
  WebView,
  StyleSheet,
} from 'react-native'

const MyWebView = ({ url }) => {
  return (
    <View style={styles.container}>
      <WebView source={{uri: url, method: 'get'}} />
    </View>
  )
}

MyWebView.propTypes = {
  url: React.PropTypes.string.isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6EF',
    flexDirection: 'column',
  },
})

export default MyWebView
