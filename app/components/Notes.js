import React from 'react-native'
import Separator from './Helpers/Separator'
import Badge from './Badge'

const {
  ListView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableHighlight,
} = React

class Notes extends React.Component {
  constructor (props) {
    super(props)
    this.ds = new ListView.DataSource({rowHasChanged: (oldRow, newRow) => oldRow !== newRow})
    this.state = {
      dataSource: this.ds.cloneWithRows(this.props.notes),
      note: '',
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }
  componentWillReceiveProps ({ notes }) {
    if (notes !== this.props.notes) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(notes),
      })
    }
  }
  onChange (e) {
    this.setState({
      note: e.nativeEvent.text,
    })
  }
  onSubmit () {
    this.props.onSubmit(this.state.note)
    this.setState({
      note: '',
    })
  }
  renderRow (rowData) {
    return (
      <View>
        <View style={styles.rowContainer}>
          <Text>{rowData}</Text>
        </View>
        <Separator />
      </View>
    )
  }
  footer () {
    return (
      <View style={styles.footerContainer}>
        <TextInput
          style={styles.searchInput}
          value={this.state.note}
          onChange={this.onChange}
          placeholder="New Note"
        />
        <TouchableHighlight
          style={styles.button}
          onPress={this.onSubmit}
          underlayerColor="#88D4F5"
        >
          <Text style={styles.buttonText}>SUBMIT</Text>
        </TouchableHighlight>
      </View>
    )
  }
  render () {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
          renderHeader={() => <Badge user={this.props.user} />}
          enableEmptySections
        />
        {this.footer()}
      </View>
    )
  }
}

Notes.propTypes = {
  notes: React.PropTypes.object.isRequired,
  user: React.PropTypes.object.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
  },
  button: {
    height: 60,
    backgroundColor: '#48BBEC',
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchInput: {
    height: 60,
    padding: 10,
    fontSize: 18,
    color: '#111',
    flex: 10,
  },
  rowContainer: {
    padding: 10,
  },
  footerContainer: {
    backgroundColor: '#E3E3E3',
    alignItems: 'center',
    flexDirection: 'row',
  },
})

export default Notes
