import React from 'react-native'

const {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableHighlight,
    ActivityIndicatorIOS,
} = React

const Main = ({ username, onChange, onSubmit, error, isLoading }) => {
  const displayError = (
    error ? <Text style={styles.error}>{error}</Text> : <View></View>
  )
  return (
    <View style={styles.mainContainer}>
      <Text style={styles.title}>Search for Github User</Text>
      <TextInput
        autoCorrect={false}
        autoCapitalize="none"
        style={styles.searchInput}
        value={username}
        onChange={onChange}
      />
      <TouchableHighlight
        style={styles.button}
        onPress={onSubmit}
        underlayerColor="white">
        <Text style={styles.buttonText}>SEARCH</Text>
      </TouchableHighlight>
      <ActivityIndicatorIOS
        style={styles.loader}
        animating={isLoading}
        color="#111"
        size="large"
      />
      {displayError}
    </View>
  )
}

Main.propTypes = {
  username: React.PropTypes.string.isRequired,
  isLoading: React.PropTypes.bool.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  error: React.PropTypes.string.isRequired,
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 30,
    marginTop: 65,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#48BBEC',
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center',
    color: '#fff',
  },
  searchInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'white',
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center',
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  loader: {
    marginTop: 10,
  },
  error: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center',
  },
})

export default Main
