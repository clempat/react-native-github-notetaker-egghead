import React, {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native'

import Badge from './Badge'
import Separator from './Helpers/Separator'

const Repositories = ({ user, repos, openPage }) => {
  const list = repos.map((item, index) => {
    const desc = repos[index].description ? <Text style={styles.description}>{repos[index].description}</Text> : <View />

    return (
        <View key={index} style={styles.container}>
          <View style={styles.rowContainer}>
            <TouchableHighlight
              onPress={openPage.bind(null, repos[index].html_url)}
              underlayerColor="transparent">
              <Text style={styles.name}>{repos[index].name}</Text>
            </TouchableHighlight>
            <Text style={styles.stars}>Stars: {repos[index].stargazers_count}</Text>
            {desc}
          </View>
          <Separator />
        </View>
    )
  })

  return (
    <ScrollView>
      <Badge user={user} />
      {list}
    </ScrollView>
  )
}

Repositories.propTypes = {
  user: React.PropTypes.object.isRequired,
  repos: React.PropTypes.array.isRequired,
  openPage: React.PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    padding: 10,
  },
  name: {
    color: '#488BEC',
    fontSize: 18,
    paddingBottom: 5,
  },
  stars: {
    color: '#488BEC',
    fontSize: 14,
    paddingBottom: 5,
  },
  description: {
    fontSize: 14,
    paddingBottom: 5,
  },
})

export default Repositories
