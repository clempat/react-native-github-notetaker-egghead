import React from 'react-native'
import Main from '../containers/Main'

const {
  NavigatorIOS,
  View,
} = React

class Scene extends React.Component {
  renderScene (route: Object, navigator: Object) {
    const Component = route.component
    return (
      <View style={{flex: 1}}>
        <Component
          navigator={navigator}
          route={route}
          {...route.passProps}
        />
      </View>
    )
  }
  render () {
    return (
      <NavigatorIOS
        style={{flex: 1}}
        renderScene={this.renderScene}
        initialRoute={{
          component: Main,
          title: 'Github Notetaker',
          backButtonTitle: 'Back',
        }}
      />
    )
  }
}

export default Scene
