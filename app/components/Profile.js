import React, {
  View,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native'

import Badge from './Badge'
import Separator from './Helpers/Separator'

const Profile = ({ user }) => {
  const dataTodisplay = ['company', 'location', 'followers', 'following', 'email', 'bio', 'public_repos']

  function getRowTitle (item) {
    const title = item.replace('_', ' ')
    return title.charAt(0).toUpperCase() + title.slice(1)
  }

  const list = dataTodisplay.map((item, index) => {
    if (!user[item]) {
      return <View key={index} />
    }

    return (
      <View key={index}>
        <View style={styles.rowContainer}>
          <Text style={styles.rowTitle}>{getRowTitle(item)}</Text>
          <Text style={styles.rowContent}>{user[item]}</Text>
        </View>
        <Separator />
      </View>
    )
  })

  return (
    <ScrollView style={styles.container}>
      <Badge user={user} />
      {list}
    </ScrollView>
  )
}

Profile.propTypes = {
  user: React.PropTypes.object.isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  rowContainer: {
    padding: 10,
  },
  rowTitle: {
    color: '#488BEC',
    fontSize: 16,
  },
  rowContent: {
    fontSize: 19,
  },
})

export default Profile
