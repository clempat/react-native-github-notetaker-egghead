import React, {
  View,
  Image,
  Text,
  StyleSheet,
} from 'react-native'

const Badge = ({ user }) => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: user.avatar_url }} />
      <Text style={styles.name}> {user.name}</Text>
      <Text style={styles.handle}>{user.login}</Text>
    </View>
  )
}

Badge.propTypes = {
  user: React.PropTypes.shape({
    avatar_url: React.PropTypes.string.isRequired,
    name: React.PropTypes.string,
    login: React.PropTypes.string.isRequired,
  }).isRequired,
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#48BBEC',
    paddingBottom: 10,
  },
  name: {
    alignSelf: 'center',
    fontSize: 21,
    marginTop: 10,
    marginBottom: 5,
    color: 'white',
  },
  handle: {
    alignSelf: 'center',
    fontSize: 16,
    color: 'white',
  },
  image: {
    height: 125,
    width: 125,
    borderRadius: 65,
    marginTop: 10,
    alignSelf: 'center',
  },
})



export default Badge
