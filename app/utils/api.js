export const getRepos = username => {
  const _username = username.toLowerCase().trim()
  const url = `https://api.github.com/users/${_username}/repos`

  return fetch(url).then(response => response.json())
}

export const getBio = username => {
  const _username = username.toLowerCase().trim()
  const url = `https://api.github.com/users/${_username}`

  return fetch(url).then(response => response.json())
}

export const getNotes = username => {
  const _username = username.toLowerCase().trim()
  const url = `https://blowing.firebaseio.com/${_username}.json`

  return fetch(url).then(response => response.json())
}

export const addNote = (username, note) => {
  const _username = username.toLowerCase().trim()
  const url = `https://blowing.firebaseio.com/${_username}.json`

  return fetch(url, {
    method: 'post',
    body: JSON.stringify(note),
  }).then(response => response.json())
}

export default {
  getRepos,
  getBio,
  getNotes,
  addNote,
}
