import api from '../utils/api'

export const ADD_NOTE_REQUEST = 'ADD_NOTE_REQUEST'
export const ADD_NOTE_SUCCESS = 'ADD_NOTE_SUCCESS'
export const ADD_NOTE_ERROR = 'ADD_NOTE_ERROR'
export const FETCH_NOTES_REQUEST = 'FETCH_NOTES_REQUEST'
export const FETCH_NOTES_SUCCESS = 'FETCH_NOTES_SUCCESS'

function addNoteRequest (username, note) {
  return {
    type: ADD_NOTE_REQUEST,
    username,
    note,
  }
}

function addNoteSuccess (username, note, id) {
  return {
    type: ADD_NOTE_SUCCESS,
    username,
    note,
    id,
  }
}

function requestNotes (username) {
  return {
    type: FETCH_NOTES_REQUEST,
    username,
  }
}

function receiveNotes (username, notes) {
  return {
    type: FETCH_NOTES_SUCCESS,
    username,
    notes,
  }
}

export function fetchNotes (username) {
  return dispatch => {
    dispatch(requestNotes(username))
    return api.getNotes(username).then(result => {
      dispatch(receiveNotes(username, result))
    })
  }
}

export function addNote (username, note) {
  const timestamp = new Date().getTime()
  return dispatch => {
    dispatch(addNoteRequest(username, {
      [timestamp]: note,
    }))
    return api.addNote(username, note).then(result => {
      dispatch(addNoteSuccess(username, { [timestamp]: note }, result.name))
    })
  }
}
