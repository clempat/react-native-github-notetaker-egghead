import api from '../utils/api'

export const FETCH_USER_BIO_REQUEST = 'FETCH_USER_REQUEST'
export const FETCH_USER_BIO_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_BIO_FAIL = 'FETCH_USER_BIO_FAIL'
export const FETCH_USER_REPOS_REQUEST = 'FETCH_USER_REPOS_REQUEST'
export const FETCH_USER_REPOS_SUCCESS = 'FETCH_USER_REPOS_SUCCESS'

function fetchUserBioRequest (username) {
  return {
    type: FETCH_USER_BIO_REQUEST,
    username,
  }
}

function fetchUserBioFail (error) {
  return {
    type: FETCH_USER_BIO_FAIL,
    error,
  }
}

function fetchUserBioSuccess (user) {
  return {
    type: FETCH_USER_BIO_SUCCESS,
    user,
  }
}

function fetchUserReposRequest (username) {
  return {
    type: FETCH_USER_REPOS_REQUEST,
    username,
  }
}

function fetchUserReposSuccess (repos) {
  return {
    type: FETCH_USER_REPOS_SUCCESS,
    repos,
  }
}

export function fetchUserBio (username) {
  return dispatch => {
    dispatch(fetchUserBioRequest(username))

    return api.getBio(username).then(result => {
      if (result.message === 'Not Found') {
        dispatch(fetchUserBioFail('User not found'))
        return
      }

      dispatch(fetchUserBioSuccess(result))
    })
  }
}

export function fetchUserRepos (username) {
  return dispatch => {
    dispatch(fetchUserReposRequest(username))

    return api.getRepos(username).then(result => {
      dispatch(fetchUserReposSuccess(result))
    })
  }
}
