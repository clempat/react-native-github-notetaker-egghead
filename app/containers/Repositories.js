import React from 'react-native'
import { connect } from 'react-redux'

import { fetchUserRepos } from '../actions/userActions'

import Repositories from '../components/Repositories'
import MyWebView from '../components/Helpers/MyWebView'

class RepositoriesContainer extends React.Component {
  constructor (props) {
    super(props)

    this.openPage = this.openPage.bind(this)
  }

  componentDidMount () {
    const {dispatch, user} = this.props
    dispatch(fetchUserRepos(user.login))
  }

  openPage (url) {
    this.props.navigator.push({
      title: 'Web View',
      component: MyWebView,
      passProps: { url },
    })
  }

  render () {
    const { user, repos } = this.props
    return <Repositories
      user={user}
      repos={repos}
      openPage={this.openPage}
    />
  }
}

RepositoriesContainer.propTypes = {
  user: React.PropTypes.object.isRequired,
  repos: React.PropTypes.array.isRequired,
  dispatch: React.PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  user: state.user.payload,
  repos: state.repos.items,
})

export default connect(mapStateToProps)(RepositoriesContainer)
