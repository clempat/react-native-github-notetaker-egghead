import React from 'react-native'
import { connect } from 'react-redux'
import Main from '../components/Main'
import Dashboard from './Dashboard'
import { fetchUserBio } from '../actions/userActions'

class MainContainer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: props.username,
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange (event) {
    this.setState({
      username: event.nativeEvent.text,
    })
  }

  onSubmit () {
    this.props.dispatch(fetchUserBio(this.state.username))
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.username !== this.props.username) {
      this.setState({
        username: nextProps.username,
      })
    }

    if (nextProps.user && nextProps.user !== this.props.user) {
      this.props.navigator.push({
        title: nextProps.user.name || 'Choose your Option',
        backButtonTitle: 'Dashboard',
        component: Dashboard,
      })
    }
  }

  render () {
    const { username } = this.state
    const { isLoading, error } = this.props
    return <Main
      username={username}
      onChange={this.onChange}
      onSubmit={this.onSubmit}
      isLoading={isLoading}
      error={error}
    />
  }

}

MainContainer.propTypes = {
  user: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  isLoading: React.PropTypes.bool.isRequired,
  error: React.PropTypes.string.isRequired,
  username: React.PropTypes.string.isRequired,
}

function mapStateToProps (state) {
  return {
    user: state.user.payload,
    username: state.user.username,
    isLoading: state.user.isFetching,
    error: state.user.error,
  }
}

export default connect(mapStateToProps)(MainContainer)
