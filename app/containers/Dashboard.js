import React from 'react-native'
import { connect } from 'react-redux'

import Profile from './Profile'
import Repositories from './Repositories'
import Notes from './Notes'

import Dashboard from '../components/Dashboard'

const DashboardContainer = ({ user, navigator }) => {
  function goToProfile () {
    navigator.push({
      title: 'Profile',
      component: Profile,
    })
  }

  function goToRepos () {
    navigator.push({
      title: 'Repositories',
      component: Repositories,
    })
  }

  function goToNotes () {
    navigator.push({
      title: 'Notes',
      component: Notes,
    })
  }

  return <Dashboard
    goToProfile={goToProfile}
    goToRepos={goToRepos}
    goToNotes={goToNotes}
    user={user}
  />
}

DashboardContainer.propTypes = {
  user: React.PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
  user: state.user.payload,
})

export default connect(mapStateToProps)(DashboardContainer)
