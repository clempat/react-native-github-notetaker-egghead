import { connect } from 'react-redux'

import Profile from '../components/Profile'

const mapStateToProps = state => ({
  user: state.user.payload,
})

export default connect(mapStateToProps)(Profile)
