import React from 'react-native'
import { connect } from 'react-redux'
import Notes from '../components/Notes'
import { addNote, fetchNotes } from '../actions/notesActions'

class NotesContainer extends React.Component {
  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
  }
  componentDidMount () {
    const { dispatch, user } = this.props

    dispatch(fetchNotes(user.login))
  }

  onSubmit (note) {
    const { dispatch, user } = this.props
    dispatch(addNote(user.login, note))
  }

  render () {
    const { notes, user } = this.props
    return <Notes notes={notes} user={user} onSubmit={this.onSubmit} />
  }
}

NotesContainer.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  user: React.PropTypes.object.isRequired,
  notes: React.PropTypes.object.isRequired,
}

function mapStateToProps (state) {
  return {
    user: state.user.payload,
    notes: state.notes.items,
    isLoading: state.notes.isFetching,
  }
}

export default connect(
  mapStateToProps
)(NotesContainer)
