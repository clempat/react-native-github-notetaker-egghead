/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React from 'react-native'
import Root from './app/containers/Root'

const { AppRegistry } = React

AppRegistry.registerComponent('githubNotetaker', () => Root)
